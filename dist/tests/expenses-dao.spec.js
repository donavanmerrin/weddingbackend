"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var connection_1 = require("../src/connection");
var expenses_dao_imple_1 = require("../src/daos/expenses-dao-imple");
var entitities_1 = require("../src/entitities");
var expensesDao = new expenses_dao_imple_1.ExpensesDaoPostgress;
test("Get all expenses", function () { return __awaiter(void 0, void 0, void 0, function () {
    var expenses, expensess;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expenses = new entitities_1.Expenses(0, "Flowers", 500, 5);
                return [4 /*yield*/, expensesDao.createExpense(expenses, expenses.weddingId)];
            case 1:
                _a.sent();
                return [4 /*yield*/, expensesDao.getAllExpenses()];
            case 2:
                expensess = _a.sent();
                expect(expensess.length).toBeGreaterThanOrEqual(1);
                return [2 /*return*/];
        }
    });
}); });
test("Get all wedding expenses", function () { return __awaiter(void 0, void 0, void 0, function () {
    var testExpense1, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                testExpense1 = new entitities_1.Expenses(0, "Florist", 1500, 5);
                return [4 /*yield*/, expensesDao.createExpense(testExpense1, testExpense1.weddingId)];
            case 1:
                _a.sent();
                return [4 /*yield*/, expensesDao.getWeddingExpensesById(5)];
            case 2:
                result = _a.sent();
                expect(result.length).toBeGreaterThanOrEqual(1);
                return [2 /*return*/];
        }
    });
}); });
test("Get expenses by Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var expense, expenses;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expense = new entitities_1.Expenses(0, "food", 1000, 2);
                return [4 /*yield*/, expensesDao.createExpense(expense, expense.weddingId)];
            case 1:
                _a.sent();
                return [4 /*yield*/, expensesDao.getAllExpenses()];
            case 2:
                expenses = _a.sent();
                expect(expenses.length).toBeGreaterThanOrEqual(1);
                return [2 /*return*/];
        }
    });
}); });
test("Create new Expense", function () { return __awaiter(void 0, void 0, void 0, function () {
    var expense1, expense;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expense1 = new entitities_1.Expenses(0, "Gifts", 1000, 5);
                return [4 /*yield*/, expensesDao.createExpense(expense1, expense1.weddingId)];
            case 1:
                _a.sent();
                return [4 /*yield*/, expensesDao.getAllExpenses()];
            case 2:
                expense = _a.sent();
                expect(expense.length).toBeGreaterThanOrEqual(1);
                return [2 /*return*/];
        }
    });
}); });
test("Update expense by id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var expense1, expenses;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expense1 = new entitities_1.Expenses(0, "Gifts", 1000, 5);
                return [4 /*yield*/, expensesDao.createExpense(expense1, expense1.weddingId)];
            case 1:
                _a.sent();
                expense1.reason = "More Gifts";
                return [4 /*yield*/, expensesDao.updateExpenseById(expense1)];
            case 2:
                expenses = _a.sent();
                expect(expenses.reason).toBe("More Gifts");
                return [2 /*return*/];
        }
    });
}); });
test("Delete expense by id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var expense, expense2, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expense = new entitities_1.Expenses(0, "Rings", 1000, 5);
                return [4 /*yield*/, expensesDao.createExpense(expense, expense.expensesId)];
            case 1:
                expense2 = _a.sent();
                return [4 /*yield*/, expensesDao.deleteExpenseById(expense2.expensesId)];
            case 2:
                result = _a.sent();
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        connection_1.client.end;
        return [2 /*return*/];
    });
}); });
