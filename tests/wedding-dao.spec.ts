import { client } from "../src/connection";
import { WeddingDao } from "../src/daos/wedding-dao";
import { WeddingDaoPostgress } from "../src/daos/wedding-dao-imple";
import { Wedding } from "../src/entitities";


const weddingDao:WeddingDao = new WeddingDaoPostgress

test("Get all weddings", async()=>{
    let wedding1:Wedding = new Wedding(0,"Sandy","10-02-2022","CA",10000)
    let wedding2:Wedding = new Wedding(0,"Eyo","02-22-2022","CA",10000)
    await weddingDao.createWedding(wedding1)
    await weddingDao.createWedding(wedding2)

    const wedding:Wedding[] = await weddingDao.getWeddings()
    expect (wedding.length).toBeGreaterThanOrEqual(2)

})
test("Get Wedding By Id", async()=>{
    let wedding:Wedding = new Wedding(0,"Jesters","04-15-2023","HI",10000)
    wedding = await weddingDao.createWedding(wedding)
    wedding.weddingId
    let getwedding:Wedding = await weddingDao.getWeddingById(wedding.weddingId)
    expect(getwedding.weddingName).toBe(wedding.weddingName)
})
test("Create new Wedding", async()=>{
    const testWedding:Wedding = new Wedding(0,"Jesters","04-15-2023","HI",10000)
    const wedding:Wedding = await weddingDao.createWedding(testWedding)
    expect(wedding).not.toBe(0)
})
test("Update Wedding By Id", async()=>{
    let wedding:Wedding = new Wedding(2,"Merrin","01-01-2027","Peru",10000)
    wedding = await weddingDao.createWedding(wedding)

    wedding.weddingName = "Mora"
    wedding = await weddingDao.updateWedding(wedding)
    expect (wedding.weddingName).toBe("Mora")
})
test("Delete wedding By ID", async()=>{
    let wedding:Wedding = new Wedding(0,"Ash","02-14-2028","CA",10000)
    let wedding2:Wedding = await weddingDao.createWedding(wedding)

    const result:boolean = await weddingDao.deleteWedding(wedding2.weddingId)
    expect(result).toBeTruthy()
})
afterAll(async()=>{
    client.end
})