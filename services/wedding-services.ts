import { Wedding } from "../src/entitities";

export default interface WeddingServices{

    getAllWeddings():Promise<Wedding[]>

    retrieveWeddingById(weddingId:number):Promise<Wedding>

    createNewWedding(wedding:Wedding):Promise<Wedding>

    updateWeddingById(wedding:Wedding):Promise<Wedding>

    deleteWeddingById(weddingId:number):Promise<boolean>
}