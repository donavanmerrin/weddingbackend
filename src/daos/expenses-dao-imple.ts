import { client } from "../connection";
import { Expenses, Wedding } from "../entitities";
import { MissingResourceError } from "../errors";
import { ExpensesDao } from "./expenses-dao";


export class ExpensesDaoPostgress implements ExpensesDao{
        
    async getAllExpenses(): Promise<Expenses[]> {
        const sql:string = "select * from expenses order by expenses_id asc";
        const result = await client.query(sql);
        const expensess:Expenses[]=[];
        for(const row of result.rows){
            const expenses:Expenses = new Expenses(
                row.expenses_id,
                row.reason,
                row.amount,
                row.w_id
                );
            expensess.push(expenses)
        }
            return expensess    
        }
    async getExpenseById(expensesId: number): Promise<Expenses> {
        const sql:string = 'select * from expenses where expenses_id = $1';
        const values = [expensesId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with ${expensesId} does not exist`)
        }
        const row = result.rows[0];
        const expenses:Expenses = new Expenses(
            row.expenses_id,
            row.reason,
            row.amount,
            row.w_id
        );
        return expenses;    
    }
    async getWeddingExpensesById(weddingId: number): Promise<Expenses[]> {

        const sql:string = "select * from expenses where w_id=$1 order by expenses_id asc"
        const values = [weddingId]
        const result = await client.query(sql, values)

        if(result.rowCount === 0){
            throw new MissingResourceError(`The expenses for wedding with the id ${weddingId} does not exist`)
        }
        const expensess:Expenses[]=[];
        for(const row of result.rows){
            const expenses:Expenses = new Expenses(
                row.expenses_id,
                row.reason,
                row.amount,
                row.w_id
                );
            expensess.push(expenses)
        }
            return expensess    
    }
    async createExpense(expenses: Expenses,weddingId:number): Promise<Expenses> {
        const sqltest = "select wedding_id from wedding where wedding_id = $1"
        const valtest = [weddingId];
        const resttest = await client.query(sqltest, valtest);
        
        const sql2:string = "insert into expenses(reason,amount,w_id) values ($1,$2,$3) returning expenses_id";
        const values2 = [expenses.reason,expenses.amount,expenses.weddingId];
        const result2 = await client.query(sql2,values2);
        expenses.expensesId = result2.rows[0].expenses_id;
        return expenses;    
    }
    async updateExpenseById(expenses: Expenses): Promise<Expenses> {
        const sql:string = 'update expenses set reason = $1, amount = $2 where expenses_id = $3';
        const values = [expenses.reason,expenses.amount,expenses.expensesId];
        const result = await client.query(sql,values);
        return expenses;    
    }
    async deleteExpenseById(expensesId: number): Promise<boolean> {
        const sql:string = 'delete from expenses where expenses_id = $1';
        const values = [expensesId];
        const result = await client.query(sql,values);  
        return true;  
    }
}